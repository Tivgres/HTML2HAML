package me.tivgres.html2haml;

import me.tivgres.html2haml.models.OwnerAuthBase;
import me.tivgres.html2haml.server.ServerLoader;

import java.io.IOException;

public class Main {

    public static String OWNER_AUTH = "default_auth";

    public static void main(String[] args){
        try {
            OWNER_AUTH = new OwnerAuthBase().getBaseAuth();
        } catch (IOException ignored) {
            System.out.println("Owner auth file not found!");
        }
        try {
            new ServerLoader(8888).start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
