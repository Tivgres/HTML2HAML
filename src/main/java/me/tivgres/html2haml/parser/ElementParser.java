package me.tivgres.html2haml.parser;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

class ElementParser {

    private final char[] specialSymbols = {'-', ' ', '/', '#', ':', '.'};

    String parseElement(Element element, int spacesCount){
        String tag = element.tagName();
        List<Attribute> attrs = element.attributes().asList();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < spacesCount; i++) {
            sb.append(" ");
        }
        if (!tag.equals("div")){
            sb.append("%").append(tag);
        }
        String className = element.attributes().get("class");
        if (className != null && !className.isEmpty()){
            sb.append(".")
                    .append(fixSpaces(className));
        }
        String id = element.attributes().get("id");
        if (id != null && !id.isEmpty()){
            sb.append("#").append(id);
        }
        List<String> names = new ArrayList<>();
        List<String> values = new ArrayList<>();
        for (Attribute attr : attrs) {
            String name = attr.getKey();
            if (!name.equals("class") && !name.equals("id") && !name.isEmpty()) {
                names.add(name);
                values.add(attr.getValue());
            }
        }
        if (!names.isEmpty()){
            sb.append("{ ");
            for (int i = 0; i < names.size(); i++) {
                if (isHaveSpecialSymbols(names.get(i))){
                    sb.append("'")
                            .append(names.get(i))
                            .append("'")
                            .append(":");
                } else {
                    sb.append(names.get(i))
                            .append(":");
                }
                sb.append(" ");

                if (isHaveSpecialSymbols(values.get(i))){
                    sb.append("'")
                            .append(values.get(i))
                            .append("'");
                } else {
                    if (isNumeric(values.get(i))){
                        sb.append(values.get(i));
                    } else {
                        sb.append(":").append(values.get(i));
                    }
                }
                if (i != names.size() - 1){
                    sb.append(",");
                }
                sb.append(" ");
            }
            sb.append("}");
        }
        if (element.ownText() != null && !element.ownText().isEmpty()){
            sb.append("\n");
            for (int i = 0; i < spacesCount; i++) {
                sb.append(" ");
            }
            sb.append("    ").append(element.ownText());
        }
        Elements elements = element.children();
        StringBuilder sbb = new StringBuilder("  ");
        int spacesCountNew = spacesCount + 2;
        for (Element element1 : elements) {
            sb.append("\n")
                    .append(sbb.toString())
                    .append(new ElementParser().parseElement(element1, spacesCountNew));
        }
        return sb.toString();
    }

    private boolean isHaveSpecialSymbols(String temp){
        StringBuilder sb = new StringBuilder(temp);
        for (int i = 0; i < sb.length(); i++) {
            char tempChar = sb.charAt(i);
            for (char specialSymbol : specialSymbols) {
                if (tempChar == specialSymbol) {
                    return true;
                }
            }
        }
        return false;
    }

    @Contract(pure = true)
    private boolean isNumeric(String temp){
        return temp.matches("-?\\d+(\\.\\d+)?");
    }

    @NotNull
    private String fixSpaces(String className){
        StringBuilder sb = new StringBuilder(className.trim());
        for (int i = 0; i < sb.length(); i++) {
            if (sb.charAt(i) == ' '){
                sb.replace(i, i + 1, ".");
            }
        }
        return sb.toString();
    }
}
