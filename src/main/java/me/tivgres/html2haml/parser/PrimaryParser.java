package me.tivgres.html2haml.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PrimaryParser {

    public String parser(String html){
        Document doc = Jsoup.parse(html);
        Elements elements = doc.body().children();
        StringBuilder sb = new StringBuilder();
        for (Element element : elements) {
            sb.append(new ElementParser().parseElement(element, 0));
            sb.append("\n");
        }
        if (sb.length() > 1){ // check length for a prevent exception
            sb.delete(sb.length() -1, sb.length()); // deleting a last of "\n", better than check index any time?
        }
        return sb.toString();
    }
}
