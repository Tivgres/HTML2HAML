package me.tivgres.html2haml.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserResponse {

    @SerializedName("success")
    private boolean isSuccess = true;

    @SerializedName("response_haml")
    private String haml;

    @SerializedName("lines_in")
    private int linesIn;

    @SerializedName("lines_out")
    private int linesOut;

    @SerializedName("symbols_in")
    private int symbolsIn;

    @SerializedName("symbols_out")
    private int symbolsOut;

    @SerializedName("error_body")
    private String error;

    @SerializedName("error_stack")
    private List<ErrorDetail> errorDetails;

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public void setLinesIn(int linesIn) {
        this.linesIn = linesIn;
    }

    public void setLinesOut(int linesOut) {
        this.linesOut = linesOut;
    }

    public void setSymbolsIn(int symbolsIn) {
        this.symbolsIn = symbolsIn;
    }

    public void setSymbolsOut(int symbolsOut) {
        this.symbolsOut = symbolsOut;
    }

    public void setHaml(String haml) {
        this.haml = haml;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setErrorDetails(List<ErrorDetail> errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getHaml() {
        return haml;
    }
}
