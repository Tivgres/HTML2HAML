package me.tivgres.html2haml.models;

import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class OwnerAuthBase {

    public OwnerAuthBase() throws IOException {
        parse(load());
    }

    private String baseAuth;

    public String getBaseAuth() {
        return baseAuth;
    }

    @NotNull
    private String load() throws IOException {
        return new String(Files.readAllBytes(Paths.get("owner_auth")));
    }

    private void parse(String fileContent){
        OwnerAuth oa = new Gson().fromJson(
                        new String(Base64.getMimeDecoder()
                                        .decode(fileContent)),
                        OwnerAuth.class);
        this.baseAuth = Base64.getEncoder()
                .encodeToString(oa.login.concat(":").concat(oa.password).getBytes());
    }

    private class OwnerAuth {

        private String login;
        private String password;

    }

    public class OwnerAuthGenerator {

        public OwnerAuthGenerator(){
            OwnerAuth oa = new OwnerAuth();
            oa.login = "some_login";
            oa.password = "some_pass";
            String json = new Gson().toJson(oa);
            String encoded = Base64.getEncoder()
                    .encodeToString(json.getBytes());
            System.out.println(encoded);
        }
    }
}
