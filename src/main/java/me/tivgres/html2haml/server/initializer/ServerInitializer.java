package me.tivgres.html2haml.server.initializer;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import me.tivgres.html2haml.server.handler.ServerHandler;

public class ServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline channelPipeline = ch.pipeline();
        channelPipeline.addLast("decoder", new HttpRequestDecoder());
        channelPipeline.addLast("encoder", new HttpResponseEncoder());
        channelPipeline.addLast("aggregator", new HttpObjectAggregator(1048576));
        channelPipeline.addLast("handler", new ServerHandler());
    }
}
