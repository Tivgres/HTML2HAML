package me.tivgres.html2haml.server.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import me.tivgres.html2haml.models.ErrorDetail;
import me.tivgres.html2haml.models.UserRequest;
import me.tivgres.html2haml.models.UserResponse;
import me.tivgres.html2haml.parser.PrimaryParser;

import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpResponseStatus.FORBIDDEN;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import static me.tivgres.html2haml.Main.OWNER_AUTH;

public class ServerHandler extends SimpleChannelInboundHandler<Object> {

    private FullHttpRequest request;
    private FullHttpResponse response;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        this.request = (FullHttpRequest) msg;
        String auth = request.headers().get("Authorization");
        if (auth != null && auth.equals(OWNER_AUTH)){
            Gson gson = new Gson();
            UserRequest userRequest = new UserRequest();
            userRequest.setHmtl(request.content()
                    .toString(
                            Charset.forName("UTF-8")));
            response = new DefaultFullHttpResponse(HTTP_1_1, OK);
            UserResponse userResponse = new UserResponse();
            // statistic
            userResponse.setLinesIn(userRequest.getHmtl().split("\n").length);
            userResponse.setSymbolsIn(userRequest.getHmtl().length());
            //
            try {
                userResponse.setHaml(new PrimaryParser().parser(userRequest.getHmtl()));
                // statistics
                userResponse.setLinesOut(userResponse.getHaml().split("\n").length);
                userResponse.setSymbolsOut(userResponse.getHaml().length());
                //
            } catch (Exception ex){
                userResponse.setSuccess(false);
                userResponse.setError(ex.toString());
                StackTraceElement[] stack = ex.getStackTrace();
                List<ErrorDetail> detailsList = new ArrayList<>();
                ErrorDetail errorDetail;
                for (StackTraceElement aStack : stack) {
                    errorDetail = new ErrorDetail();
                    errorDetail.setErrorTrace(aStack.toString());
                    detailsList.add(errorDetail);
                }
                userResponse.setErrorDetails(detailsList);
            }
            response.content().writeBytes(gson.toJson(userResponse).getBytes());
            response.headers().set(CONTENT_TYPE, "application/json");
        } else {
            response = new DefaultFullHttpResponse(HTTP_1_1, FORBIDDEN);
        }
        if (HttpUtil.isKeepAlive(request)) {
            response.headers().set(CONNECTION, HttpHeaderValues.CLOSE);
        }
        response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
        ctx.writeAndFlush(response);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
